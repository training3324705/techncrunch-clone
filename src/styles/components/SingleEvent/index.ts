import styled from 'styled-components'
interface IBtn {
  a: any
}

export const SingleEventMainWrapper = styled.div`
  margin-left: 15vw;
`
export const TitleAndDateWrapper = styled.div`
  background-color: black;
  color: #fff;
  padding: 1.2vw;
`
export const Title = styled.div`
  /* border: 1px solid red; */
  width: 70vw;
  font-size: 7vw;
  font-weight: 800;
  border-bottom: 1px solid grey;
`

export const CityAndContentWrapper = styled.div`
  display: flex;
  gap: 2vw;
  padding: 1vw;
`

export const CityAndDateWrapper = styled.div`
  font-size: 2vw;
  font-weight: 800;
`
export const City = styled.div``
export const Date = styled.div``
export const Content = styled.div`
  /* border: 1px solid grey; */
  width: 45vw;
  font-size: 1.25vw;
`

export const InfoWrapper = styled.div`
  width: 70vw;
`
export const InfoTitle = styled.div`
  font-size: 5vw;
  font-weight: 800;
`
export const InfoDescription = styled.div`
  color: #777;
  font-size: 2vw;
`

export const Line = styled.div`
  height: 2px;
  width: 60px;
  margin: 20px 0;
  background-color: #14c435;
`
export const NewsMainWrapper = styled.div<IBtn>`
  display: flex;
  justify-content: space-between;
  width: 70vw;
  /* border: 1px solid red; */
  flex-direction: ${({ a }) => a || 'row'};
  padding: 1vw 0;
`
export const TitleAndDescriptionWrapper = styled.div`
  width: 40vw;
`
export const NewsTitle = styled.div`
  font-size: 1.5vw;
  font-weight: 800;
`
export const NewsDescription = styled.div`
  font-size: 1vw;
  font-weight: 300;
  color: #777;
`
export const ImageWrapper = styled.div`
  width: 20vw;
`
export const MediaImg = styled.img`
  width: 20vw;
`

export const Policy = styled.div`
  width: 40vw;
  font-size: 1vw;
  font-weight: 300;
  padding: 1vw 0;
`
