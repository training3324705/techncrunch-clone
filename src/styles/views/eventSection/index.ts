import styled from 'styled-components'

export const MainEventsWrapper = styled.div`
  margin-left: 15vw;
  /* background-image: linear-gradient(135deg, rgb(122, 220, 180) 0%, rgb(0, 208, 130) ); */
  background-image: linear-gradient(to right, #3cb371, #32cd32);
`

export const EventsHeading = styled.div`
  color: #fff;
  font-size: 1.5vw;
  font-weight: 900;
  padding: 1.2vw;
`
export const EventsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
  width: 70vw;
  color: #fff;
  padding: 1vw;
  border-bottom: 1px solid hsla(0, 0%, 100%, 0.35);
`
export const EventsDateWrapper = styled.div`
  font-size: 0.9vw;
`
export const EventsTitleWrapper = styled.div`
  font-size: 1vw;
  font-weight: 800;
`
export const EventsCityAndCountryWrapper = styled.div`
  font-size: 0.9vw;
`
export const EventsButtonsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 1vw;
`
export const BuyButtonWrapper = styled.div``
export const BuyButton = styled.button`
  color: #000;
  background-color: #ddd;
  padding: 17px 30px;
  border: none;
  font-size: 0.8vw;
  font-family: aktiv-grotesk, sans-serif;
  font-weight: 700;
  cursor: pointer;
  outline: 0;
`
export const SponsorButtonWrapper = styled.div``
export const SponsorButton = styled.button`
  border-color: #000;
  color: #000;
  background-color: transparent;
  padding: 15px 30px;
  font-size: 0.8vw;
  font-family: aktiv-grotesk, sans-serif;
  font-weight: 700;
  cursor: pointer;
  :hover {
    color: #fff;
    background-color: #000;
  }
`

export const PastEventsMainWrapper = styled.div`
  margin-left: 14.5vw;
  width: 70vw;
`
export const PastEventsHeadng = styled.div`
  font-size: 1.5vw;
  font-weight: 900;
  padding: 1vw 0;
  border-bottom: 2px solid #f1f1f1;
`
export const PastEventsWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 1vw 0;
  :nth-of-type(odd) {
    background-color: #f8f8f8;
  }
`

export const PastEventsContent = styled.div`
  font-size: 1vw;
  font-weight: 900;
  color: #00a562;
  padding: 0.7vw 0;
  :hover {
    cursor: pointer;
    /* border-bottom: 1px solid green; */
  }
`

export const PastEventsDate = styled.div`
  font-size: 0.8vw;
  color: #777;
`
