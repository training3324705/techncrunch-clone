import styled from 'styled-components'

export const Main = styled.div`
  background-color: lightgrey;
  width: 60vw;
  height: 20vh;
  border-radius: 30px;
  box-shadow: 10px 10px 5px lightblue;
`
export const Heading = styled.h1`
  text-shadow: 2px 2px;
  text-align: center;
  color: white;
`

export const Content = styled.p`
  color: black;
`
