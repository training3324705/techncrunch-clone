import useGet from 'hooks/useGet'
import { useEffect, useState } from 'react'
import moment from 'moment'
import {
  LatestNewsContent,
  LatestNewsImg,
  LatestNewsImgWrapper,
  LatestNewsTitle,
  LatestNewsTitleAuthDateWrapper,
  LatestNewsWrapper,
  NewsAuthor,
  NewsDate,
  LoadMoreWrapper,
  LoadMoreButton,
} from 'styles/views/topNews/index'
import { useNavigate } from 'react-router-dom'
import styled from 'styled-components'
import Loader from 'components/Loader'

const AppsSection = () => {
  const [page, setPage] = useState(1)
  const [perPagePost, setPerPagePost] = useState(20)
  const {
    refetch: fetchDetails,
    data,
    isLoading,
    error,
    isFetching,
  } = useGet(
    'apps-length',
    `https://techcrunch.com//wp-json/wp/v2/posts?category_slug=apps&per_page=${perPagePost}&page=${page}`,
  )
  //   console.log('security data', data)
  const loadMoreData = () => {
    if (page == 5) {
      setPage(1)
      setPerPagePost(perPagePost - 80)
    } else {
      setPage(page + 1)
      setPerPagePost(perPagePost + 20)
    }
  }

  const navigate = useNavigate()

  useEffect(() => {
    fetchDetails()
  }, [page])

  if (error) return <h1>Error...</h1>
  const handleclickItem = (item: any) => {
    navigate('/singlepost', { state: item })
  }

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <MainAppsWrapper>
          <AppsHeading>Apps</AppsHeading>
          <AppsTitle>
            The app economy continues to grow, having produced a record number of downloads and consumer spending across
            both the iOS and Google Play stores. Keep up with this fast-moving industry in one place, with the latest
            from the world of apps, including news, updates, startup fundings, mergers and acquisitions, and much more.
          </AppsTitle>
          {data?.map((item: any, index: number) => {
            const AuthorName = 'parsely-author'
            const content = item.excerpt.rendered
            const contentData = content.slice(3, content.length - 15)
            const Title = item.title.rendered

            return (
              <LatestNewsWrapper key={index} onClick={() => handleclickItem(item)}>
                <LatestNewsTitleAuthDateWrapper>
                  <LatestNewsTitle dangerouslySetInnerHTML={{ __html: Title }}></LatestNewsTitle>
                  <NewsAuthor>{item.parselyMeta[AuthorName]}</NewsAuthor>
                  <NewsDate>{moment(item.date).format('LLL')}</NewsDate>
                </LatestNewsTitleAuthDateWrapper>
                <LatestNewsContent dangerouslySetInnerHTML={{ __html: contentData }}></LatestNewsContent>
                <LatestNewsImgWrapper>
                  <LatestNewsImg src={item.jetpack_featured_media_url} />
                </LatestNewsImgWrapper>
              </LatestNewsWrapper>
            )
          })}
          <LoadMoreWrapper>
            <LoadMoreButton onClick={loadMoreData}>{isFetching ? 'loading' : 'Load More'}</LoadMoreButton>
          </LoadMoreWrapper>
        </MainAppsWrapper>
      )}
    </>
  )
}

export default AppsSection

// styles
export const MainAppsWrapper = styled.div`
  margin-left: 15vw;
`
export const AppsHeading = styled.div`
  font-size: 5vw;
  font-weight: 800;
`
export const AppsTitle = styled.div`
  font-size: 1vw;
  font-weight: 400;
  width: 45vw;
  color: black;
`
