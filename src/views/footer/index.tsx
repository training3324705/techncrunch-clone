import React from 'react'
import {
  AboutHeading,
  AboutLinks,
  AboutWrapper,
  FooterWrapper,
  LegalWrapper,
  LegalHeading,
  LegalLinks,
  AboutandLinksWrapper,
  LegalandLinksWrapper,
  TechandLinksWrapper,
  TechWrapper,
  TechHeading,
  TechLinks,
  SocialIconsTextWrapper,
  IconsTextWrapper,
  FbIconTextWrapper,
  TwIconsTextWrapper,
  FbIconText,
  SocialText,
  TwIconsText,
  BottomTextWrapper,
} from 'styles/views/footer'
import { GrFacebookOption } from 'react-icons/gr'
import { AiFillYoutube } from 'react-icons/ai'
import { FaLinkedinIn, FaTwitter, FaInstagram } from 'react-icons/fa'

const Footer = () => {
  return (
    <FooterWrapper>
      <AboutandLinksWrapper>
        <AboutWrapper>
          <AboutHeading>About</AboutHeading>
          <AboutLinks>TechCrunch</AboutLinks>
          <AboutLinks>Staff</AboutLinks>
          <AboutLinks>Contact Us</AboutLinks>
          <AboutLinks>Advertise</AboutLinks>
          <AboutLinks>CrunchBoard Jobs</AboutLinks>
          <AboutLinks>Site Mapp</AboutLinks>
        </AboutWrapper>
      </AboutandLinksWrapper>
      <LegalandLinksWrapper>
        <LegalWrapper>
          <LegalHeading>Legal</LegalHeading>
          <LegalLinks>Privacy Policy</LegalLinks>
          <LegalLinks>Terms Of Service</LegalLinks>
          <LegalLinks>TechCrunch+ Terms</LegalLinks>
          <LegalLinks>Code Of Conduct</LegalLinks>
          <LegalLinks>About Our Ads</LegalLinks>
        </LegalWrapper>
      </LegalandLinksWrapper>
      <TechandLinksWrapper>
        <TechWrapper>
          <TechHeading>Trending Tech Topics</TechHeading>
          <TechLinks>TechIndustry Layoffs</TechLinks>
          <TechLinks>Google Search on Event 2022</TechLinks>
          <TechLinks>Amazon Fall Event 2022</TechLinks>
        </TechWrapper>
      </TechandLinksWrapper>
      <SocialIconsTextWrapper>
        <IconsTextWrapper>
          <FbIconTextWrapper>
            <FbIconText>
              <GrFacebookOption className="icons" />
              <SocialText>Facebook</SocialText>
            </FbIconText>
            <FbIconText>
              <AiFillYoutube className="icons" />
              <SocialText>YouTube</SocialText>
            </FbIconText>
            <FbIconText>
              <FaLinkedinIn className="icons" />
              <SocialText>LinkedIn</SocialText>
            </FbIconText>
          </FbIconTextWrapper>
          <TwIconsTextWrapper>
            <TwIconsText>
              <FaTwitter className="icons" />
              <SocialText>Twitter</SocialText>
            </TwIconsText>
            <TwIconsText>
              <FaInstagram className="icons" />
              <SocialText>Instagram</SocialText>
            </TwIconsText>
          </TwIconsTextWrapper>
        </IconsTextWrapper>
      </SocialIconsTextWrapper>
      <BottomTextWrapper>
        © 2022 Yahoo.All rights reserved.Powered by WordPress VIP(opens in a new window).
      </BottomTextWrapper>
    </FooterWrapper>
  )
}

export default Footer
