import useGet from 'hooks/useGet'
import { useEffect, useState } from 'react'
import moment from 'moment'
import {
  LatestNewsContent,
  LatestNewsImg,
  LatestNewsImgWrapper,
  LatestNewsTitle,
  LatestNewsTitleAuthDateWrapper,
  LatestNewsWrapper,
  NewsAuthor,
  NewsDate,
  LoadMoreWrapper,
  LoadMoreButton,
} from 'styles/views/topNews/index'

import { useNavigate } from 'react-router-dom'
import styled from 'styled-components'
import Loader from 'components/Loader'

const Security = () => {
  const [page, setPage] = useState(1)
  const [perPagePost, setPerPagePost] = useState(20)
  const {
    refetch: fetchDetails,
    data,
    isLoading,
    error,
    isFetching,
  } = useGet('security-length', `posts?category_slug=security&per_page=${perPagePost}&page=${page}`)
  //   console.log('security data', data)
  const loadMoreData = () => {
    if (page == 5) {
      setPage(1)
      setPerPagePost(perPagePost - 80)
    } else {
      setPage(page + 1)
      setPerPagePost(perPagePost + 20)
    }
  }

  const navigate = useNavigate()

  useEffect(() => {
    fetchDetails()
  }, [page])

  if (error) return <h1>Error...</h1>
  const handleclickItem = (item: any) => {
    navigate('/singlepost', { state: item })
  }

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <MainSecurityWrapper>
          <SecurityHeading>Security</SecurityHeading>
          <SecurityTitle>
            Cybersecurity encompasses investigative reporting and analysis on the latest security breaches, hacks and
            cyberattacks around the globe.
          </SecurityTitle>
          {data?.map((item: any, index: number) => {
            const AuthorName = 'parsely-author'
            const content = item.excerpt.rendered
            const contentData = content.slice(3, content.length - 15)
            const Title = item.title.rendered

            return (
              <LatestNewsWrapper key={index} onClick={() => handleclickItem(item)}>
                <LatestNewsTitleAuthDateWrapper>
                  <LatestNewsTitle dangerouslySetInnerHTML={{ __html: Title }}></LatestNewsTitle>
                  <NewsAuthor>{item.parselyMeta[AuthorName]}</NewsAuthor>
                  <NewsDate>{moment(item.date).format('LLL')}</NewsDate>
                </LatestNewsTitleAuthDateWrapper>
                <LatestNewsContent dangerouslySetInnerHTML={{ __html: contentData }}></LatestNewsContent>
                <LatestNewsImgWrapper>
                  <LatestNewsImg src={item.jetpack_featured_media_url} />
                </LatestNewsImgWrapper>
              </LatestNewsWrapper>
            )
          })}
          <LoadMoreWrapper>
            <LoadMoreButton onClick={loadMoreData}>{isFetching ? 'loading' : 'Load More'}</LoadMoreButton>
          </LoadMoreWrapper>
        </MainSecurityWrapper>
      )}
    </>
  )
}

export default Security

// styles
export const MainSecurityWrapper = styled.div`
  margin-left: 15vw;
`
export const SecurityHeading = styled.div`
  font-size: 5vw;
  font-weight: 800;
`
export const SecurityTitle = styled.div`
  font-size: 1vw;
  font-weight: 400;
  width: 45vw;
  color: black;
`
