import React from 'react'
import styled from 'styled-components'

const TrendingNews = () => {
  return (
    <TopNewsWrapper>
      <TopNewsTitleandImgWrapper>
        <TopNewsTitleWrapper>
          <TopNewsTitle>1</TopNewsTitle>
        </TopNewsTitleWrapper>
        <TopNewsAuthorWrapper>
          <TopNewsAuthor>2</TopNewsAuthor>
        </TopNewsAuthorWrapper>
        <TopNewsImgWrapper>
          <TopNewsImg></TopNewsImg>
        </TopNewsImgWrapper>
      </TopNewsTitleandImgWrapper>

      <TopNewsTitlesWrapper>
        <div>3</div>
        <div>4</div>
        <div>5</div>
        <div>6</div>
      </TopNewsTitlesWrapper>
    </TopNewsWrapper>
  )
}

export default TrendingNews

// styles
export const TopNewsWrapper = styled.div`
  display: flex;
  gap: 2.5vw;
`

export const TopNewsTitleandImgWrapper = styled.div`
  width: 41vw;
  /* border: 2px solid red; */
`

export const TopNewsTitleWrapper = styled.div``

export const TopNewsTitle = styled.div``

export const TopNewsAuthorWrapper = styled.div``

export const TopNewsAuthor = styled.div``

export const TopNewsImgWrapper = styled.div``

export const TopNewsImg = styled.img``

export const TopNewsTitlesWrapper = styled.div`
  width: 24.5vw;
  /* border: 2px solid red; */
`
